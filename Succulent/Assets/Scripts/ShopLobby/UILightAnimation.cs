﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILightAnimation : MonoBehaviour
{
    public float animateTime = .05f;
    public float lightStep = .01f;
    public float minLight = .5f;
    private Image image;
    private bool flag = true;
    private float a = 0f;
    private Coroutine cr;
    
    void Start()
    {
        image = GetComponent<Image>();
        cr = StartCoroutine("Animate");
    }
    
    void OnEnable()
    {
        cr = StartCoroutine("Animate");
    }

    void OnDisable()
    {
        StopCoroutine(cr);
    }

    private IEnumerator Animate()
    {
        while (true)
        {
            yield return new WaitForSeconds(animateTime);
            image.color = new Color(1f, 1f, 1f, a);
            if(flag)
            {
                a += lightStep;
                if(a > 1f) flag = !flag;
            }
            else
            {
                a -= lightStep;
                if (a < minLight) flag = !flag;
            }
        }

    }
}
