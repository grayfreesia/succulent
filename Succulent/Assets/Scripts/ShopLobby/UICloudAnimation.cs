﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICloudAnimation : MonoBehaviour
{
    public float animateTime = .5f;
    public float speed = 200f;

    public RectTransform rt;
    public float width = 0f;
    public Coroutine cr;

    void Start()
    {
        rt = GetComponent<RectTransform>();
        width = rt.sizeDelta.x;
        cr = StartCoroutine("Animate");
    }

    void OnEnable()
    {
        cr = StartCoroutine("Animate");
    }
    void OnDisable()
    {
        StopCoroutine(cr);
    }

    private IEnumerator Animate()
    {
        while (true)
        {
            yield return new WaitForSeconds(animateTime);
            transform.Translate(Vector3.right * speed * Time.deltaTime);
            if(rt.localPosition.x > width)
            {
                rt.localPosition = new Vector2(0f, rt.localPosition.y);
            }
        }

    }
}
