﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAnimation : MonoBehaviour
{
    public Sprite[] sprites;
    public float animateTime = .05f;
    private Image image;
    private int num = 0;
    private int leng = 0;
    private Coroutine cr;
    void Start()
    {
        image = GetComponent<Image>();
        leng = sprites.Length;
        cr = StartCoroutine("Animate");
    }

    void OnEnable()
    {
        cr = StartCoroutine("Animate");
    }
    
    void OnDisable()
    {
        StopCoroutine(cr);
    }

    private IEnumerator Animate()
    {
        while (true)
        {
            yield return new WaitForSeconds(animateTime);
            image.sprite = sprites[num++ % leng];
            if(num == leng) num = 0;
        }

    }
}
