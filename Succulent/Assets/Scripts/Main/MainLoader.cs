﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainLoader : MonoBehaviour
{
    public bool isDay = true;
    public GameObject day;
    public GameObject night;

    void Awake()
    {
        SetDayNight();
    }

    public void SetDayNight()
    {
        if(TimeManager.Instance == null)
        {
            if(Random.Range(0,2) == 0) isDay = true;
            else isDay = false;
        }
        else
        {
            isDay = TimeManager.Instance.isDay;
        }
        if(isDay) 
        {
            day.SetActive(true);
            night.SetActive(false);
        }
        else 
        {
            day.SetActive(false);
            night.SetActive(true);
        }
    }
}
