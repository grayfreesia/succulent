﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public int day = 0;
    public float timeSpeed = 1.0f;
    public float time = 0f;
    public bool isDay = true;
    public static TimeManager Instance{get; private set;}

    void Awake()
    {
        if(Instance == null) Instance = this;
    }

    void Update()
    {
        time += timeSpeed * Time.deltaTime;
        if(time > 1f)
        {
            time = 0f;
            isDay = !isDay;
            if(isDay)
            {
                day++;
            }

            //Swap day2night or night2day
            MainLoader ml = FindObjectOfType<MainLoader>();
            if(ml != null) ml.SetDayNight();
            DayNight dn = FindObjectOfType<DayNight>();
            if(dn != null) dn.SwapDayNight();

        }
    }

}
