﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    public static PlayerData Instance{get; private set;}

    public int windowNum = 0;
    public int potNum = 0;
    public int itemNum = 0;
    public bool [] windows = new bool[31];
    public bool [] pots = new bool[18];
    public bool [] items = new bool[43];
    void Awake()
    {
        if(Instance == null) Instance = this;
    }

    public void BuyItem(int kind, int num)
    {
        if(kind == 0)//window
        {
            windowNum = num;
            windows[num] = true;
        }
        else if(kind == 1)//pot
        {
            potNum = num + 1;
            pots[num + 1] = true;
        }
        else//item
        {
            itemNum = num;
            items[num] = true;
        }
    }


}
