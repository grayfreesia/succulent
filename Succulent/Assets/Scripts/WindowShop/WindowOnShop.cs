﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindowOnShop : MonoBehaviour
{
    public int num = 0;
    public Image img;

    void Awake()
    {
        num = transform.GetSiblingIndex();
        GetComponent<Button>().onClick.AddListener(OnClickWIndow);
        img = GetComponentsInChildren<Image>()[1];
    }

    void OnClickWIndow()
    {
        WindowBuyPanel.Instance.winNum = num;
        WindowBuyPanel.Instance.num = num * 3;
        WindowBuyPanel.Instance.img.sprite = img.sprite;
        WindowBuyPanel.Instance.img.SetNativeSize();
        WindowBuyPanel.Instance.SetColorPanels();
    }

}
