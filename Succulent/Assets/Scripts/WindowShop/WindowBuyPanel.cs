﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindowBuyPanel : MonoBehaviour
{
    public int affinity = 0;
    public int winNum = 0;
    public int num = 0;
    public Sprite [] sprites;
    public Color [] colors;
    public static WindowBuyPanel Instance{get; private set;}

    public Image img;
    public GameObject [] colorPanels;
    public GameObject lastWindow;
    void Awake()
    {
        if(Instance == null) Instance = this;
    }

    void Start()
    {
        OnAffinityChanged();
    }

    public void OnClickColor(int color)
    {
        num = winNum * 3 + color;
        if(num > 30) num = 30;
        img.sprite = sprites[num];
    }

    public void SetColorPanels()
    {        
        for(int i = 0; i < colorPanels.Length; i++)
        {
            colorPanels[i].GetComponent<Image>().color = colors[num+i];
        }
    }

    public void OnAffinityChanged()
    {
        for(int i = 0; i < colorPanels.Length; i++)
        {
            colorPanels[i].SetActive(i<=affinity? true : false);
        }
        lastWindow.SetActive(affinity>2? true : false);
    }

    public void OnBuy()
    {
        PlayerData.Instance.BuyItem(0, num);
    }

}
