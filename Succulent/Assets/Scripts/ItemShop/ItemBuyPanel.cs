﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBuyPanel : MonoBehaviour
{
    public int num = 0;
    public bool isItem;
    public void OnClickYes()
    {
        //재화 감소
        //저장
        PlayerData.Instance.BuyItem(isItem?2:1, num);
        ClosePanel();
    }

    public void OnClickNo()
    {
        ClosePanel();
    }

    private void ClosePanel()
    {
        gameObject.SetActive(false);
    }

    public void OpenPanel()
    {
        gameObject.SetActive(true);
    }
}
