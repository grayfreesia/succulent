﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemOnShop : MonoBehaviour
{
    public int num;
    public GameObject Panel;
    private ItemBuyPanel itemBuyPanel;

    void Start()
    {
        itemBuyPanel = Panel.GetComponent<ItemBuyPanel>();
    }

    public void OnClickItem()
    {
        itemBuyPanel.OpenPanel();
        itemBuyPanel.num = num;
    }
}
