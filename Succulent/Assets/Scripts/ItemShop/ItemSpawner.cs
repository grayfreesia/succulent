﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSpawner : MonoBehaviour
{
    public int affinity = 0;
    public GameObject [] places;
    [System.Serializable]
    public class ItemByAffinity{
        public Sprite [] sprites;
    }
    public ItemByAffinity [] items;
    List<int> selects = new List<int>();
    
    private Sprite [] sprites;
    private int numByAffinity = 0;
    void Start()
    {
        sprites = items[affinity].sprites;
        for(int i = 0; i < items.Length; i++)
        {
            if(i == affinity) break;
            numByAffinity += items[i].sprites.Length;
        }
        Spwan();
    }
    //랜덤 3개 뽑아서 배치
    public void Spwan()
    {
        selects.Clear();
        if(sprites.Length < places.Length || sprites.Length == 0) return;//무한루프 방지
        for(int i = 0; i < places.Length; i++)
        {
            int num = -1;
            
            while(num < 0 || selects.Contains(num))
            {
                num = Random.Range(0,sprites.Length);
            }
            selects.Add(num);

            Image img = places[i].GetComponent<Image>();
            img.sprite = sprites[num];
            //img.SetNativeSize();
            places[i].GetComponent<ItemOnShop>().num = num + numByAffinity;

        }
    }
}
