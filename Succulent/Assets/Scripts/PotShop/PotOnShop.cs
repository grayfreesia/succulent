﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PotOnShop : MonoBehaviour
{
    public int num = 0;
    public ItemBuyPanel buyPanel;

    void Awake()
    {
        GetComponent<Button>().onClick.AddListener(OnClickPot);
    }

    private void OnClickPot()
    {
        buyPanel.OpenPanel();
        buyPanel.num = num;
    }

}
