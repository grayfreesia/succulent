﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PotSpawner : MonoBehaviour
{
    public int affinity = 0;
    [System.Serializable]
    public class PotByAffinity{
        public GameObject [] pots;
    }
    public PotByAffinity [] pots;
    
    void Awake()
    {
        for(int i = 0; i < affinity+1; i++)
        {
            for(int j = 0; j < pots[i].pots.Length; j++)
            {
                pots[i].pots[j].GetComponent<Button>().interactable = true;
            }
        }
    }
    
    
}
