﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomSpawner : MonoBehaviour
{
    public Image window;
    public Image pot;
    public Image item;

    public Sprite [] wSprites;
    public Sprite [] pSprites;
    public Sprite [] iSprites;

    void Start()
    {
        RenderRoom();
    }

    void RenderRoom()
    {
        window.sprite = wSprites[PlayerData.Instance.windowNum * 2 + (TimeManager.Instance.isDay? 0 : 1)];
        window.SetNativeSize();
        pot.sprite = pSprites[PlayerData.Instance.potNum];
        pot.SetNativeSize();
        item.sprite = iSprites[PlayerData.Instance.itemNum];
        item.SetNativeSize();
    }
}
