﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayNight : MonoBehaviour
{
    public Image fillWhite;
    public Image fillYellow;
    public GameObject day;
    public GameObject night;

    void Start()
    {
        SwapDayNight();
    }

    void Update()
    {
        if(TimeManager.Instance.isDay)
        {
            fillWhite.fillAmount = TimeManager.Instance.time;
        }
        else
        {
            fillYellow.fillAmount = TimeManager.Instance.time;
        }
    }

    public void SwapDayNight()
    {
        if(TimeManager.Instance.isDay)
        {
            day.SetActive(true);
            night.SetActive(false);
            fillYellow.transform.SetAsFirstSibling();;
            fillYellow.fillAmount = 1f;
        }
        else
        {
            night.SetActive(true);
            day.SetActive(false);
            fillWhite.transform.SetAsFirstSibling();;
            fillWhite.fillAmount = 1f; 
        }
    }

}
