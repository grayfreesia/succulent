﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButton : MonoBehaviour
{
    public bool isOpen = false;
    public GameObject MenuBG;

    public void OnClickToggle()
    {
        transform.localScale = new Vector3(isOpen?-1f:1f, 1f, 1f);
        isOpen = !isOpen;
        MenuBG.SetActive(isOpen);
    }
}
